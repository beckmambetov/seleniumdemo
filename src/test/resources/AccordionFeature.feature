@Feature_Accordion
Feature: accordion test

  @Accordion @DefaultFunctionality @High
  Scenario: Default functionality section 1 select
    Given I'm on main page
    And I select "Accordion" category
    When I select "Section 1" section
    And I select "Section 2" section
    And I select "Section 3" section
    And I select "Section 4" section
    Then I check that current section is "Section 4"

  @Accordion @CustomizeIcons @High
  Scenario: Customize icons section 2 select
    Given I'm on main page
    And I select "Accordion" category
    And I select "Customize icons" tab
    When I select "Section 2" section
    And I check that icons are visible
    And I click on 'Toggle icons' button
    And I check that icons are not visible
    And I click on 'Toggle icons' button
    And I check that icons are visible

  @Accordion @FillSpace @High
  Scenario: Fill Space section 1 select
    Given I'm on main page
    And I select "Accordion" category
    And I select "Fill Space" tab
    When I select "Section 1" section
    And I select "Section 2" section
    And I select "Section 3" section
    And I select "Section 4" section
    Then I check that current section is "Section 4"