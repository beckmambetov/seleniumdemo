package stepdefinition;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import arp.CucumberArpReport;
import arp.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import pages.MainPage;
import pages.base.PageInstance;

/**
 * Created by pchelintsev on 5/17/2016.
 */
public class MainPageStepDefinitions extends PageInstance {
    @Autowired
    private
    MainPage mainPage;

    @Given("^I'm on main page$")
    public void iMOnMainPage() throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(4000);
            ReportService.ReportAction("I`m on a main page", mainPage.titleHome.isDisplayed());
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I select \"([^\"]*)\" category$")
    public void iSelectCategory(String categoryName) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            mainPage.getCategory(categoryName).click();
            ReportService.ReportAction("Question was selected", true);
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }

    @And("^I click on \"([^\"]*)\" button$")
    public void iClickOnButton(String buttonName) throws Throwable {
        if (checkIfFurtherStepsAreNeeded()) {
            return;
        }
        try {
            Thread.sleep(1000);
            mainPage.getButton(buttonName).click();
        } catch (AssertionError e) {
            throw e;
        } catch (Throwable e) {
            ReportService.ReportAction(e.getMessage(), false);
        } finally {
            CucumberArpReport.nextStep();
        }
    }


}
