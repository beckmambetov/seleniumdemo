package testUtils;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import helpers.SystemHelper;
import arp.CucumberArpReport;
import arp.ReportService;
import pages.base.PageInstance;

import static com.unitedsofthouse.ucucumberpackage.typesfactory.factory.TypeFactory.setArpReportClient;
import static helpers.SystemHelper.MAINWINDOWHANDLER;
import static helpers.SystemHelper.PROJECTKEY;
import static helpers.SystemHelper.Reset_Values;

//import pages.LoginPage;

/**
 * Created by logovskoy on 12/3/2015.
 * This class is for Cucumber internal use
 */
public class BeforeAfter extends PageInstance {

    public static Scenario lastScenario;

    /**
     * This method resets values to default before each scenario
     * In case if current scenario belongs to different feature than the one that was run previously
     * Then report is sent to reporting service and a new report is created
     */
    @Before
    public void setUp(Scenario scenario) {
        Reset_Values();
        try {
            MAINWINDOWHANDLER = driver.getWindowHandle();
            setArpReportClient(new CucumberArpReport());
            if (!scenario.getId().startsWith(CucumberArpReport.getTestSuiteName().toLowerCase())) {
                ReportService.closeAndSendToAnotherURL("http://10.10.80.162:90/Services/Deployment.asmx?WSDL");
                CucumberArpReport.open(PROJECTKEY, scenario.getId());
            }
            CucumberArpReport.addTestToTestSuite(scenario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method returns user to homepage after each scenario
     */
    @After
    public void tearDown(Scenario scenario) throws Exception {
        try {
            CucumberArpReport.decideTestStatus();
            CucumberArpReport.FinishTest();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            driver.navigate().to(SystemHelper.URL);
        }
    }
}