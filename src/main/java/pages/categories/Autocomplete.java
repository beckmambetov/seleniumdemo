package pages.categories;

import com.unitedsofthouse.ucucumberpackage.typesfactory.types.TextInput;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.springframework.context.annotation.Lazy;
import pages.base.BasePage;

/**
 * Created by ivnytska on 18-May-16.
 */
public class Autocomplete extends BasePage {
    @Override
    protected WebElement elementForLoading() throws Exception {
        return null;
    }

    @Lazy
    @FindBy(xpath = ".//*[@id='tagss']")
    public TextInput queryStringDefault;

    @Lazy
    @FindBy(xpath = ".//*[@id='tagsss']")
    public TextInput queryStringMultipleValues;

    @Lazy
    @FindBy(xpath = ".//*[@id='searcha']")
    public TextInput queryStringCategories;

    /**
     *This method enter text in search string in 'Default functionality' view
     *
     * @param text
     * @throws Exception
     */

    public void enterTextDefault(String text) throws Exception {
        queryStringDefault.clear();
        queryStringDefault.sendKeys(text);
    }

    /**
     * This method select required item from autocomplete results
     *
     * @param text
     * @throws InterruptedException
     */

    public void selectText(String text) throws InterruptedException {
        driver.findElement(By.linkText(text)).click();
    }

    /**
     * This method check that correct item from autocomplete results was selected in 'Default functionality' section
     *
     * @param text
     * @throws Exception
     */

    public void checkTextTrueDefault(String text) throws Exception {
        Assert.assertEquals(queryStringDefault.getText(), text);
    }

    /**
     * This method enter text in search string in 'Multiple Values' section
     *
     * @param text
     * @throws Exception
     */

    public void enterTextMultipleValues(String text) throws Exception {
        queryStringMultipleValues.sendKeys(text);
    }

    /**
     * This method check that correct items from autocomplete results were selected in 'Multiple Values' section
     *
     * @param text
     * @throws Exception
     */

    public void checkTextTrueMultipleValues(String text) throws Exception {
        Assert.assertEquals(queryStringMultipleValues.getText().substring(0, queryStringMultipleValues.getText().length() - 2), text);
    }

    /**
     * This method enter text in search string in 'Categories' section
     *
     * @param text
     * @throws Exception
     */

    public void enterTextCategories(String text) throws Exception {
        queryStringCategories.clear();
        queryStringCategories.sendKeys(text);
    }

    /**
     * This method check that correct item from autocomplete results was selected in 'Categories' section
     *
     * @param text
     * @throws Exception
     */

    public void checkTextTrueCategories(String text) throws Exception {
        Assert.assertEquals(queryStringCategories.getText(), text);
    }
}